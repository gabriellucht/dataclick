<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Socio extends Model
{
    protected $table = 'socio';

    protected $fillable = ['nome'];

    public function clubes()
    {
        return $this->belongsToMany(Clube::class)->withTimestamps();
    }
}
