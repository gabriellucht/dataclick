<?php

namespace App\Http\Controllers;

use App\Models\Clube;
use App\Http\Requests\ClubeRequest;

class ClubeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clubes = Clube::orderBy('nome');
        if($nome = request()->get('nome')){
            $clubes->where('nome','like','%'.$nome.'%');
        }
        $clubes = $clubes->paginate();

        return view('clubes.listar',compact('clubes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clubes.criar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ClubeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClubeRequest $request)
    {
        $dados =$request->all();
        $clube = Clube::create($dados);
        return redirect()->route('clubes.index')->with(['mensagem_sucesso'=>'Cadastrado com sucesso!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Clube $clube)
    {
        return view('clubes.exibir',compact('clube'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Clube $clube)
    {
        return view('clubes.criar',compact('clube'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ClubeRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClubeRequest $request,Clube $clube)
    {
        $clube->fill($request->all());
        return redirect()->route('clube.index')->with(['mensagem_sucesso'=>'Cadastrado com sucesso!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clube $clube)
    {
        $clube->delete();
        return response()->json(['deletado'=>1,'clube'=>$clube]);
    }
}
