<?php

namespace App\Http\Controllers;

use App\Http\Requests\SocioRequest;
use App\Models\Socio;
use App\Models\Clube;

class SocioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $socios = Socio::orderBy('nome');
        if($nome = request()->get('nome')){
            $socios->where('nome','like','%'.$nome.'%');
        }
        $socios = $socios->paginate();
        return view('socios.listar',compact('socios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clubes = Clube::orderBy('nome')->get();
        return view('socios.criar',compact('clubes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\SocioRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SocioRequest $request)
    {
        $socio = Socio::create($request->all());
        if($clubes = $request->get('clube_id')){
            $socio->clubes()->attach($clubes);
        }
        return redirect()->route('socios.index')->with(['mensagem_sucesso'=>'Cadastrado com sucesso!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Socio $socio)
    {
        return view('socios.exibir',compact('socio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Socio $socio)
    {
        return view('socios.criar',compact('socio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\SocioRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SocioRequest $request,Socio $socio)
    {
        $socio->fill($request->all());
        return redirect()->route('socios.index')->with(['mensagem_sucesso'=>'Cadastrado com sucesso!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Socio $socio)
    {
        $socio->delete();

        return response()->json(['deletado'=>1,'socio'=>$socio]);
    }
}
