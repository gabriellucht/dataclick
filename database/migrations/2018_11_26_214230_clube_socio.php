<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClubeSocio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clube_socio', function (Blueprint $table) {
            $table->integer('clube_id')->unsigned()->nullable();
            $table->foreign('clube_id')->references('id')
                    ->on('clube')->onDelete('cascade');

            $table->integer('socio_id')->unsigned()->nullable();
            $table->foreign('socio_id')->references('id')
                  ->on('socio')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
