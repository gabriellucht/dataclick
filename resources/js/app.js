require('./bootstrap');
require('bootstrap-select');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    $('[data-link-delete]').click(function (e) {
        let link = $(this).data('link-delete'), tipoElemento = $(this).data('tipo-elemento')
        $.ajax({
            type: 'delete',
            url: link,
            success: function (response) {
                $('#' + tipoElemento + '-' + response[tipoElemento].id).remove()
            }
        })
    })
    $(".bs-select").selectpicker()
})
