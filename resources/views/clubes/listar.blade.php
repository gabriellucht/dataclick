@extends('master')

@section('conteudo')
<div class="card">
    <div class="card-header">
        <h1>Clubes</h1>
    </div>
    <div class="card-body">
        @if(session('mensagem_sucesso'))
        <div class="row">
            <div class="alert alert-success alert-dismissible fade show w-100" role="alert">
                {{session('mensagem_sucesso')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col m-1">
                <form><input type="text" class="form-control" name="nome" placeholder="São Paulo.."></form>
            </div>
            <div class="col m-1">
            <div class="float-right">
                    <a href="{{route('clubes.create')}}" class="btn btn-primary">Cadastrar</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <table class="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Sócios</th>
                    <th scope="col" class="text-right">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($clubes as $clube)
                    <tr id="clube-{{$clube->id}}">
                        <td scope="row">{{$loop->index +1 }}</td>
                        <td>{{$clube->nome}}</td>
                        <td>{{$clube->socios()->count()}}</td>
                        <td class="text-right"><button data-link-delete="{{route('clubes.destroy',$clube)}}" data-tipo-elemento="clube" class="delete btn btn-danger">Deletar</button></td>
                    </tr>
                    @empty
                        <tr ><td class="text-center" colspan="4">Sem clubes cadastrados</td></tr>
                    @endforelse
                </tbody>
                </table>
                <div class="col">
                    {{$clubes->links()}}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
