@extends('master')

@section('conteudo')
    <div class="card">
        <div class="card-header">
            Cadastrar clube
        </div>
        <div class="card-body">
            <form action="{{route('clubes.store')}}" method="post">
            {{csrf_field()}}
                <div class="form-row">
                    <div class="col mb-3">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" placeholder="Nome" id="nome" value="" name="nome" required>
                    </div>
                </div>
                <div class="form-row">
                    <hr>
                    <button class="btn btn-primary" type="submit">Salvar</button>
                </div>
            </form>
        </div>
    </div>
@append
