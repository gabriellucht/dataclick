@extends('master')

@section('conteudo')
    <div class="card">
        <div class="card-header">
            Cadastrar Sócio
        </div>
        <div class="card-body">
            <form action="{{route('socios.store')}}" method="post">
            {{csrf_field()}}
                <div class="form-row">
                    <div class="col mb-3">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" placeholder="Nome" id="nome" value="{{$socio->nome ?? null}}" name="nome" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col mb-3">
                        <label for="nome">Clube</label>
                        <select class="bs-select" data-size="8" data-live-search="true" name="clube_id[]" multiple="multiple">
                            <option value="">Selecione um clube</option>
                            @foreach($clubes as $clube)
                                <option @if(isset($socio) && $socio->clubes->contains($clube->id)) selected  @endif value="{{$clube->id}}">{{$clube->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <hr>
                    <button class="btn btn-primary" type="submit">Salvar</button>
                </div>
            </form>
        </div>
    </div>
@append
